!! ------------------------------------------------------------------ !!
!! Procedures : Type Barge
!! ------------------------------------------------------------------ !!
Subroutine Initialize_typSurfMeshHemisphere( this, json )
!! -------------------------------------------------------------------------- !!
    Implicit None
    class(typSurfMeshHemisphere), intent(inout) :: this
    Type(typJSON), Pointer                      :: json
!! -------------------------------------------------------------------------- !!
    Real(RP), allocatable :: centerMove(:)

    !!... Get Information
    Call JSON_GetReal( json, "radius", this%radius )
    Call JSON_GetInt( json, "nX", this%Nx )
    Call JSON_GetInt( json, "nZ", this%Nz )

    Call JSON_GetRealOrDefault( json, "rX", this%rX, 1.0_RP )
    Call JSON_GetRealOrDefault( json, "rZ", this%rZ, 1.0_RP )

    Call JSON_GetRealVectorOrDefault( json, "centerMove", centerMove, [0.0_RP, 0.0_RP, 0.0_RP] )
    If ( size(centerMove).EQ.3 ) then
        this%centerMove(:) = centerMove(:)
    else
        Call GURU%Error( &
        &   msg  = "The dimension of given 'centerMove' is not 3.", &
        &   head = "Initialize_typSurfMeshHemisphere" )
    end if
    If (Allocated(centerMove)) deallocate(centerMove)

    !!... Create Mesh
    Call this%CreateMesh()

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!

!! -------------------------------------------------------------------------- !!
Subroutine CreateMesh_typSurfMeshHemisphere(this)
!! -------------------------------------------------------------------------- !!
    Implicit None
    Real(RP),Dimension(3), parameter :: refVector = [0.0_RP, 0.0_RP, 1.0_RP]
    class(typSurfMeshHemisphere), intent(inout) :: this
!! -------------------------------------------------------------------------- !!
    Real(RP), Allocatable :: nodePos(:, :)
    Integer, allocatable  :: panelConnect(:, :)
    Integer :: iNode, iPanel
    Real(RP), Dimension(3) :: center, relVect
    Real(RP) :: magnitude

    !!... Associate Barge
    ascBarge: Associate( barge => this%bargeMesh )

    !!... Set Barge Information
    barge%Lx = this%radius
    barge%Ly = this%radius
    barge%Lz = this%radius

    barge%nX = this%nX
    barge%nY = this%nX
    barge%nZ = this%nZ

    barge%rX = this%rX
    barge%rY = this%rX
    barge%rZ = this%rZ

    barge%centerMove(:) = this%centerMove(:)

    barge%isOpenTop = .TRUE.

    !!... Create Mesh
    Call barge%CreateMesh()

    !!... Create temporal dataset
    Allocate( nodePos(3, barge%nNode) )
    Allocate( panelConnect(4, barge%nPanel) )

    do iNode = 1, barge%nNode
        nodePos(:, iNode) = barge%node(iNode)%Vec()
    end do

    do iPanel = 1, barge%nPanel
        do iNode = 1, 4
            panelConnect(iNode, iPanel) = barge%panel(iPanel)%NodeLabel(iNode)
        end do
    end do

    center(:) = this%centerMove(:)

    !!... Project to Hemisphere
    do iNode = 1, barge%nNode

        !!... Relative Vector
        relVect(:) = nodePos(:, iNode) - center(:)

        magnitude = dsqrt( relVect(1) * relVect(1) &
        &                + relVect(2) * relVect(2) &
        &                + relVect(3) * relVect(3) )

        nodePos(:, iNode) = center(:) + relVect(:) * this%radius / ( magnitude + 1.D-16 )

    end do

    !!... Manual Set
    Call this%typSurfMesh%ManualSet( &
    &   nodePos      = nodePos,       &
    &   panelConnect = panelConnect   )

    deallocate( nodePos, panelConnect )

    End Associate ascBarge

!! -------------------------------------------------------------------------- !!
End Subroutine
!! -------------------------------------------------------------------------- !!
