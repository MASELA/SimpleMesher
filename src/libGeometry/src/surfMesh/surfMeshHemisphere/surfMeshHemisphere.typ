!!*------------------------------------------------------------------*!!
!!  Project : mkSurfaceMesh
!!*------------------------------------------------------------------*!!
!!
!!  Fortran Type
!!      typParaPipeMesh
!!
!!  Description
!!      typParaPipeMesh Type
!!
!!  Authors
!!      YoungMyung Choi, Ecole Centrale de Nantes
!!          - youngmyung.choi@ec-nantes.fr
!!
!!*------------------------------------------------------------------*!!

!! ------------------------------------------------------------------ !!
!!                      Type typParaPipeMesh
!! ------------------------------------------------------------------ !!

    type,extends(typSurfMesh), public :: typSurfMeshHemisphere

!! Type Data -------------------------------------------------------- !!

    private

        !! - Radius
        Real(RP) :: radius

        !! - Nx, Ny, Nz
        Integer  :: Nx, Nz

        !! - Origin
        Real(RP), dimension(3) :: centerMove

        !! - Ratio to Increment
        Real(RP) :: rX, rZ

        !! - Barge Mesh
        Type(typSurfMeshBarge), Private :: bargeMesh

!! Type Procedures -------------------------------------------------- !!

    contains

        !!... Initialize the class
        Procedure :: Initialize => Initialize_typSurfMeshHemisphere

        !!... Create Mesh
        Procedure, Private :: CreateMesh => CreateMesh_typSurfMeshHemisphere

    end type

!! ------------------------------------------------------------------ !!
!!                      Type Point
!! ------------------------------------------------------------------ !!
